# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""


'''import math

def volumecuboid(l,b,h):
    return(l*b*h)'''

import sys, re
print('This program plots a circle and a square and prints out its area : ')

#circle
import numpy as np
PI = np.pi
x = np.linspace(-1,1,25)
radius = float(input('Enter radius of circle : '))

import pylab as pl
print('Drawing the circle...')
pl.plot(x, np.sqrt(radius ** 2 - x ** 2), lw = 2.0, label = 'circle')

area_circ = PI * radius ** 2

#square
side = float(input('Enter side of square : '))
print('Drawing the square...')
x = np.linspace(-side / 2, side / 2, 25)
pl.plot(x, x, lw = 2.0, label = 'Square')

area_sq = side ** 2
print('Area of square with side %3.2f is : %5.2f' %(side, area_sq))
 
pl.legend(loc = 0) 
pl.show()
